This is a demo for using TuxSuite to run tc tests.

tc-x86.yaml is a simple plan file which has 1 build with tc related configs and 1 test that executes tc-tests.


full.yaml is a full plan which builds and run tc-tests with clang-nightly and clang-17 toolchain for x86 and arm64 architecture.


Please email to us on tuxsuite@linaro.org for TuxSuite Token or fill the form here: https://forms.gle/3NaW5fuNykGstsMq6

More details here: https://tuxsuite.com/

For information on the TuxMake, TuxBake, and TuxRun open source projects see:
https://gitlab.com/Linaro/tuxmake
https://gitlab.com/Linaro/tuxbake
https://gitlab.com/Linaro/tuxrun

Dashboard visualization:
https://qa-reports.linaro.org/~anders.roxell/tc-testing-demo/

For information on the TuxSuite cloud service https://docs.tuxsuite.com/


Signup for a trial at https://tuxsuite.com or email tuxsuite@linaro.org


For more information, please contact us at tuxsuite@linaro.org


Here is the demo execution:
![Demo](demo.gif)
